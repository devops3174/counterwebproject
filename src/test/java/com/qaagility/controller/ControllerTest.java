package com.qaagility.controller; 
import static org.junit.Assert.*;
import org.junit.Test;

public class ControllerTest {
    @Test
    public void calculateTest() throws Exception {

        int k= new Controller().calculate(5, 0);
        assertEquals("Case 0", Integer.MAX_VALUE, k);
        
    }

    @Test
    public void calculateTest2() throws Exception {

        int k= new Controller().calculate(5, 1);
        assertEquals("Case 1", 5, k);

    }



}
